package myPacage;


public class Cosmetic {
    private String title;
    private int year;
    private boolean iHaveIt = true;
    private int godenDo;


    public void godenDo() {
        if (year < 2020) {
            System.out.println("Не рискуй! Купи новое.");
        } else {
            System.out.println("Все ОК. Можно пользоваться!");
        }
    }

    public void iHaveIt() {
        if (iHaveIt == true) {
            System.out.println("У меня есть!!!");
        } else {
            System.out.println("Закончилось :(");
        }
    }

    public int getYear() {
        return year;
    }

    public int getGodenDo() {
        return godenDo;
    }

    public boolean isIHaveIt() {
        return iHaveIt;
    }

    public String getTitle() {
        return title;
    }


    public Cosmetic(String title, int year, boolean iHaveIt) {

        this.title = title;
        this.year = year;
        this.iHaveIt = iHaveIt;
    }

}

