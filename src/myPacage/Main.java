package myPacage;

import java.util.Scanner;
import java.time.Year;
import java.util.Scanner;


enum Brend {
    LANCOME,
    YVES_SAINT_LAURENT,
    ORIFLAME
}

interface Decorative {
    public void paint();
}

interface Caring {
    public void care();
}

abstract class cosmeticStore {
    public abstract void sellingCosmetic();
}

public class Main {
    public static void main(String[] args) {

        System.out.println("Сколько тебе лет?");

        Party party = new Party(2);
        party.checkAge();

        System.out.println();

        Cosmetic lipstick = new Cosmetic("Lipstick", 2021, true);
        System.out.print(lipstick.getTitle() + " ");
        System.out.println(Brend.LANCOME);
        lipstick.godenDo();
        lipstick.iHaveIt();

        Lipstick lipstick1 = new Lipstick();
        lipstick1.paint();

        System.out.println();

        Cosmetic ink = new Cosmetic("Ink", 2019, true);
        System.out.print(ink.getTitle() + " ");
        System.out.println(Brend.YVES_SAINT_LAURENT);
        ink.godenDo();
        ink.iHaveIt();

        Ink ink1 = new Ink();
        ink1.paint();

        System.out.println();

        Cosmetic toneCream = new Cosmetic("Tone cream", 2022, false);
        System.out.print(toneCream.getTitle() + " ");
        System.out.println(Brend.ORIFLAME);
        toneCream.godenDo();
        toneCream.iHaveIt();

        Tone_cream tone_cream1 = new Tone_cream();
        tone_cream1.paint();

        System.out.println();

        Makeup_remover makeup_remover = new Makeup_remover();
        System.out.print("Средство для снятия макияжа ");
        makeup_remover.care();

        System.out.println();

        Brocard brocard = new Brocard();
        brocard.sellingCosmetic();
    }

}
